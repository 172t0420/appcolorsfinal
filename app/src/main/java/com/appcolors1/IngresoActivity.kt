package com.appcolors1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_ingreso.*

class IngresoActivity : AppCompatActivity() {

    private val GOOGLE_SING_IN = 100

    private val callbackManager = CallbackManager.Factory.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_AppColors1)

        super.onCreate(savedInstanceState)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_ingreso)

        btnIniciar.setOnClickListener {
            abrirMenu()
        }

        btnCrear.setOnClickListener {
            abrirRegistrar()
        }

        btnGoogle.setOnClickListener {
            accederGoogle()
        }

        btnFacebook.setOnClickListener {
            abrirFacebook()
        }
    }

    fun accederGoogle(){
        val googleConfig = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        val googleClient = GoogleSignIn.getClient(this, googleConfig)
        googleClient.signOut()
        startActivityForResult(googleClient.signInIntent,GOOGLE_SING_IN)
    }

    fun abrirFacebook(){
        LoginManager.getInstance().logInWithReadPermissions(this, listOf("email"))

        LoginManager.getInstance().registerCallback(callbackManager,
        object: FacebookCallback<LoginResult>{
            override fun onSuccess(result: LoginResult?) {
                result?.let {
                    val token = it.accessToken

                    val credencial = FacebookAuthProvider.getCredential(token.token)
                    FirebaseAuth.getInstance().signInWithCredential(credencial).addOnCompleteListener{
                        if(it.isSuccessful){
                            mostrarMenu(it.result?.user?.email?: "", ProviderType.FACEBOOK)
                        }else{
                            mostrarAlert()
                        }
                    }
                }
            }

            override fun onError(error: FacebookException?) {
                TODO("Not yet implemented")
            }

            override fun onCancel() {
                mostrarAlert()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode,resultCode,data)

        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == GOOGLE_SING_IN){
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try{
                val account = task.getResult(ApiException::class.java)
                if(account != null){
                    val credencial = GoogleAuthProvider.getCredential(account.idToken,null)
                    FirebaseAuth.getInstance().signInWithCredential(credencial).addOnCompleteListener{
                        if(it.isSuccessful){
                            mostrarMenu(it.result?.user?.email?: "", ProviderType.BASIC)
                        }else{
                            mostrarAlert()
                        }
                    }
                }
            }catch (e: ApiException){
                mostrarAlert()
            }
        }

    }

    fun abrirRegistrar(){
        startActivity(Intent(this, RegistrarActivity::class.java))
    }

    fun abrirMenu(){
        val usuario = txtUsu.text.toString()
        val contrasenia = txtPass.text.toString()

        if(usuario.isEmpty() || contrasenia.isEmpty()){
            Toast.makeText(this,"Ingrese los datos correspondientes",Toast.LENGTH_SHORT).show()
        }else{
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(usuario, contrasenia).addOnCompleteListener{
                    if(it.isSuccessful){
                        mostrarMenu(it.result?.user?.email?: "", ProviderType.BASIC)
                    }else{
                        mostrarAlert()
                    }
                }
        }
    }

    fun mostrarAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Se ha producido un error al autentificarse")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun mostrarMenu(correo: String, proveedor: ProviderType){
        val intent = Intent(this, MenuActivity::class.java).apply {
            putExtra("correo",correo)
            putExtra("proveedor",proveedor.name)
        }

        startActivity(intent)
    }
}