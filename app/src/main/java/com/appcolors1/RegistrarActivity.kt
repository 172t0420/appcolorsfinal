package com.appcolors1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_registrar.*
import java.security.Provider

class RegistrarActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_registrar)
        btnIngresar.setOnClickListener {
            registrar()
        }

    }

    private fun registrar() {
        val usuario2 = txtUsuario.text.toString()
        val contrasenia2 = txtContrasena.text.toString()
        val confirmar = txtConfirmar.text.toString()
        val correo = txtCorreo.text.toString()
        val check = checkBoxAceptar

        if(usuario2.isEmpty()){
            Toast.makeText(this,"Ingrese el usuario", Toast.LENGTH_SHORT).show()
            //return@setOnClickListener
        }
        if(contrasenia2.isEmpty()){
            Toast.makeText(this,"Ingrese una contraseña", Toast.LENGTH_SHORT).show()
            //return@setOnClickListener
        }
        if(contrasenia2.length < 6){
            Toast.makeText(this,"La contraseña debe de ser mayor a 5 caracteres", Toast.LENGTH_SHORT).show()
            //return@setOnClickListener
        }
        if(confirmar.isEmpty()){
            Toast.makeText(this,"Verifique la contraseña", Toast.LENGTH_SHORT).show()
            //return@setOnClickListener
        }else if(confirmar != contrasenia2){
            Toast.makeText(this,"Las contraseñas no son iguales. Verifique por favor", Toast.LENGTH_SHORT).show()
            //return@setOnClickListener
        }
        if(correo.isEmpty()){
            Toast.makeText(this,"Ingrese un correo electronico", Toast.LENGTH_SHORT).show()
            //return@setOnClickListener
        }
        if(check.isChecked){
            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(correo, contrasenia2).addOnCompleteListener{
                    if(it.isSuccessful){
                        mostrarMenu(it.result?.user?.email?: "", ProviderType.BASIC)
                    }else{
                        mostrarAlert()
                    }
                }
        }else{
            Toast.makeText(this,"Acepte los terminos y servicios", Toast.LENGTH_SHORT).show()
            //return@setOnClickListener
        }
    }

    fun mostrarAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Se ha producido un error al registrarse")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun mostrarMenu(correo: String, proveedor: ProviderType){
        val intent = Intent(this, MenuActivity::class.java).apply {
            putExtra("correo",correo)
            putExtra("proveedor",proveedor.name)
        }

        startActivity(intent)
    }
}