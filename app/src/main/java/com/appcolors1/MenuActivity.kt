package com.appcolors1

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_ingreso.*
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_registrar.*

enum class ProviderType{
    BASIC,
    GOOGLE,
    FACEBOOK
}

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        imageProductos.setOnClickListener {
            startActivity(Intent(this,ProductosActivity::class.java))
        }
        imageColores.setOnClickListener{
            startActivity(Intent(this,ColorsActivity::class.java))
        }
        imageCamara.setOnClickListener{
            Toast.makeText(this,"Aún no esta disponible está opción", Toast.LENGTH_SHORT).show()
        }

        btnCerrar.setOnClickListener{
            cerrarSesion()
        }
    }

    fun cerrarSesion(){
        FirebaseAuth.getInstance().signOut()

        LoginManager.getInstance().logOut()

        startActivity(Intent(this,IngresoActivity::class.java))
    }
}